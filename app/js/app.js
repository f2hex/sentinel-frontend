const pdata = {
 
    type: 'video',
    sources: [
        {
            src:  "videos/video.mp4",
            type: "video/mp4",
            size: 720
        }
  ],
  title: "Sentinel netcam",
  debug: true,
    keyboard: {
        global: true
    },
    tooltips: {
        controls: true
    },
    captions: {
        active: true
    },
    autoplay: true,
    poster: "videos/poster.jpg",
    previewThumbnails: { enabled: true, src: "videos/thumbs.vtt" }
};

/*
const player1 = new Plyr("#Scilla");
const player2 = new Plyr("Cariddi");
const player3 = new Plyr("#Tuke");
const player4 = new Plyr("#Polifemo");
const player5 = new Plyr("#Geryon");
player1.source = data;
player2.source = data;
player3.source = data;
player4.source = data;
player5.source = data;
*/

var netcams = [];

var create_video_url = function(http, vdate, vtime) {
		var vurl = "videos/scilla/" + moment(vdate).format("YYYY-MM-DD") + "_" + moment(vtime).format("HH-MM") + ".mp4";
		console.log("New selected video: " + vurl);
		$http.head(vurl).
				then(function(resp) {
						console.log("HEAD success");
						pdata.sources[0].src = vurl;
						netcams["scilla"].player.source = pdata;
				}, function(resp) {		 
						console.log("HEAD failure");
				});
};


var app = angular.module("Sentinel", [
    "ngMaterial",
    "ngMessages",
    "md.time.picker"
]);

app.config([
    "$mdThemingProvider",
    function($mdThemingProvider) {
        "use strict";
        $mdThemingProvider.theme("default").primaryPalette("blue");
    }
]);

function round(date, duration, method) {
    x = moment(Math[method]((+date) / (+duration)) * (+duration));
		return x.toDate();
}

/*
 * date picker controller: validate section of date within allowed
 * range
 */
app.controller("videoBrowser", function($scope, $http) {
    $scope.vDate = new Date();
		// round to the nearest 5 mins
		$scope.vTime = round($scope.vDate, moment.duration(5, "minutes"), "floor");
    $scope.minDate = new Date(
        $scope.vDate.getFullYear(),
        $scope.vDate.getMonth() - 2,
        $scope.vDate.getDate()
		);
    $scope.maxDate = $scope.vDate;
    $scope.onlyWeekendsPredicate = function(date) {
        var day = date.getDay();
        return day === 0 || day === 6;
    }
		// manage the change of the date or time
		$scope.dtChange = function() {
        dd = moment($scope.vDate).format("YYYY-MM-DD");
				tt = moment($scope.vTime).format("HH:mm");
				dt = moment(dd + " " + tt);
				var vurl = "videos/scilla/" + moment(dt).format("YYYY-MM-DD_HH-mm") + ".mp4";
				console.log("New selected video: " + vurl);
				try {
						$http.head(vurl).
								then(function(resp) {
										console.log("HEAD success");
										pdata.sources[0].src = vurl;
										netcams["scilla"].player.source = pdata;
								}, function(resp) {		 
										console.log("video not found");
								});
				} catch(err) {
						console.log("=====>")
						// catch 404 errors
				}
		}
		
});

/*
 * time picker controller: validate section of date within allowed
 * range
 */
app.controller("videoTimePicker", function($scope) {
});

app.component("netcamsList", {
    template:
    '<md-tabs md-dynamic-height="" md-border-bottom="">' +
        ' <md-tab label="{{netcam.label}}" ng-repeat="(name,netcam) in $ctrl.ncdata.netcams">' +
        '   <video id="{{name}}" controls crossorigin playsinline></video>' +
        ' </md-tab>' +
        '</md-tabs>',
    controller: function netcamsController($http, $timeout) {
        var self = this;
        $http.get('data/netcams.json').then(function(response) {
            self.ncdata = response.data;
            $timeout(function() {
                for (var nc_name in self.ncdata.netcams) {
                    nc = self.ncdata.netcams[nc_name];
                    console.debug("netcams: " + nc.label);
                    nc.player = new Plyr('#' + nc_name);
                    //self.netcams[nx].player.source(pdata);
                    nc.player.source = pdata;
                    console.debug(pdata);
                }
                netcams = self.ncdata.netcams;
            },10);
        });
    }
});


