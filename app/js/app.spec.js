'use strict';


describe("netcamsController", function() {

    beforeEach(module("Sentinel"));

    it('should create a `netcams` model with 5 netcams', inject(function($controller) {
        var scope = {};
        var ctrl = $controller"netcamsController", {$scope: scope});
        expect(scope.phones.length).toBe(3);
    }));
    
});
